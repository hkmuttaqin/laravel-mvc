<?php

namespace App\Http\Controllers;

use App\Book;
use App\BookType;
use App\Publisher;
use App\Writer;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::get();
        return view('book.index', ['books' => $books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bookTypes = BookType::get();
        $publishers = Publisher::get(); 
        $writers = Writer::get();

        return view('book.create',[
           'bookTypes' => $bookTypes,
           'publishers' => $publishers,
           'writers' => $writers,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book = new Book;
        $book -> bok_book_type_id = $request->book_type_id;
        $book -> bok_writer_id = $request->writer_id;
        $book -> bok_publisher_id = $request->publisher_id;
        $book -> bok_name = $request->name;
        $book -> bok_stock = $request->stock;
        $book -> bok_published_date  = $request->date;
        $book -> save();

        return redirect(url('/books'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        $Book = new Book;
        $book = $Book->getDetailBook($book->bok_id);

        if ($book) {
            return view('book.show', [
                'book' => $book
            ]);
        } else {
            abort(404);
        }       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        $bookTypes = BookType::get();
        $publishers = Publisher::get(); 
        $writers = Writer::get();

        return view('book.edit',[
            'book'=>$book,
            'bookTypes' => $bookTypes,
            'publishers' => $publishers,
            'writers' => $writers,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $book = Book::where('bok_id', $request->book_id)->first();
        $book -> bok_book_type_id = $request->book_type_id;
        $book -> bok_writer_id = $request->writer_id;
        $book -> bok_publisher_id = $request->publisher_id;
        $book -> bok_name = $request->name;
        $book -> bok_stock = $request->stock;
        $book -> bok_published_date  = $request->date;
        $book -> save();

        if ($book) {
            return redirect(url('/books/'. $book ->bok_id));
        } else {
            abort(404);
        }       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        if ($book) {
            $book->delete();
            return redirect(url('/books'));
        }else{
            abort(404);
        }
    }
}
