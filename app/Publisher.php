<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publisher extends Model
{
    protected $primaryKey = 'pub_id';
    use SoftDeletes;
}
