<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    protected $primaryKey = 'bok_id';
    use SoftDeletes;

    public function bookType()
    {
        return $this->belongsTo('App\BookType', 'bok_book_type_id', 'bot_id');
    }

    public function writer()
    {
        return $this->belongsTo('App\Writer', 'bok_writer_id', 'wrt_id');
    }

    public function publisher()
    {
        return $this->belongsTo('App\Publisher', 'bok_publisher_id', 'pub_id');
    }

    public function getDetailBook($bookID){
        $detailBook = Book::Join('book_types','book_types.bot_id', 'books.bok_book_type_id')
                    ->Join('writers','writers.wrt_id', 'books.bok_writer_id')
                    ->Join('publishers','publishers.pub_id', 'books.bok_publisher_id')
                    ->where('bok_id', $bookID)
                    ->first();
        return $detailBook;
    }
}
