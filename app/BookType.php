<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookType extends Model
{
    protected $primaryKey = 'bot_id';
    use SoftDeletes;
}
