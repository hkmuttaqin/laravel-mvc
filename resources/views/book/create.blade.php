@extends('templates/base') {{-- mengambil file base.blade.php --}}
@section('title','Tambah Buku')
@section('container') {{-- Mengisi di bagian content --}}
<!-- Main Section -->
<div class="container">
    <div class="row">
        <!-- Content -->
        <div class="col-md-12 mt-3">
            <h3>Form Menambah Buku</h3>
            <form action="/books" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Nama Buku</label>
                    <input class="form-control" type="text" name="name" id="name" placeholder="Masukkan Nama Buku" required>
                </div>
                <div class="form-group">
                    <label for="type">Tipe Buku</label>
                    <select class="form-control" name="book_type_id" id="type" required>
                        <option value="">Masukkan Tipe Buku</option>
                        @foreach ($bookTypes as $bookType)
                            <option value="{{$bookType->bot_id}}">{{$bookType->bot_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="stock">Stock</label>
                    <input class="form-control" type="number" name="stock" id="stock" placeholder="Masukkan stock buku" required>
                </div>
                <div class="form-group">
                    <label for="writer">Penulis</label>
                    <select class="form-control" name="writer_id" id="writer" required>
                        <option value="">Masukkan Penulis Buku</option>
                        @foreach ($writers as $writer)
                            <option value="{{$writer->wrt_id}}">{{$writer->wrt_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="publisher">Penerbit</label>
                    <select class="form-control" name="publisher_id" id="publisher" required>
                        <option value="">Masukkan Penerbit Buku</option>
                        @foreach ($publishers as $publisher)
                            <option value="{{$publisher->pub_id}}">{{$publisher->pub_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="date">Waktu Terbit</label>
                    <input class="form-control" type="date" name="date" id="date" required>
                </div>
                <div class="form-group float-right">
                    <button class="btn btn-lg btn-danger" type="reset">Reset</button>
                    <button class="btn btn-lg btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.content -->
    </div>
</div>
<!-- /.Main Section -->
@endsection