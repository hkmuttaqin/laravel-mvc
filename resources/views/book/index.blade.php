@extends('templates/base')
@section('title','Books Database')
@section('container')
<div class="container">
    <div class="row">
        <div class="my-4 col-12">
            <h1 class="float-left">Daftar Buku</h1>
            <a class="btn btn-primary float-right mt-2" href="{{url('/books/create')}}" role="button">Tambah Buku</a>
        </div>
        <div class="col-12">
            <table class="table table-stripped">
                <thead class="thead-primary">
                    <tr>
                        <th class="text-center">No</th>
                        <th>Nama Buku</th>
                        <th>Tipe Buku</th>
                        <th class="text-center">Stock</th>
                        <th>Penulis</th>
                        <th>Penerbit</th>
                        <th class="text-center">Terbit</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($books as $book)
                    <tr>
                        <td class="text-center">{{$loop->iteration}}</td>
                        <td>{{$book->bok_name}}</td>
                        <td>{{$book->bookType->bot_name}}</td>
                        <td class="text-center">{{$book->bok_stock}}</td>
                        <td>{{$book->writer->wrt_name}}</td>
                        <td>{{$book->publisher->pub_name}}</td>
                        <td class="text-center">{{$book->bok_published_date}}</td>
                        <td>
                            <a href="{{url('/books/'.$book->bok_id)}}" class="btn btn-xs btn-primary">Detail</a> |
                            <a href="{{url('/books/'.$book->bok_id.'/edit')}}" class="btn btn-xs btn-warning">Edit</a> |
                            <a href="{{url('/books/'.$book->bok_id.'/destroy')}}" class="btn btn-xs btn-danger" onclick="return confirm('yakin?');">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection