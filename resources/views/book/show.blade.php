@extends('templates/base') {{-- mengambil file base.blade.php --}}
@section('title','Detail Buku')
@section('container') {{-- Mengisi di bagian content --}}
<!-- Main Section -->
<div class="container">
    <div class="row">
        <!-- Content -->
        <div class="col-md-12 mt-3">
            <h3>Detail Buku</h3>
            <dl class="row">
                <dt class="col-sm-3">Nama Buku</dt>
                <dd class="col-sm-9">{{$book->bok_name}}</dd>
              
                <dt class="col-sm-3">Tipe Buku</dt>
                <dd class="col-sm-9">{{$book->bot_name}}</dd>
              
                <dt class="col-sm-3">Stock</dt>
                <dd class="col-sm-9">{{$book->bok_stock}}</dd>
              
                <dt class="col-sm-3 text-truncate">Penulis</dt>
                <dd class="col-sm-9">{{$book->wrt_name}}</dd>

                <dt class="col-sm-3 text-truncate">Penerbit</dt>
                <dd class="col-sm-9">{{$book->pub_name}}</dd>

                <dt class="col-sm-3 text-truncate">Waktu Terbit</dt>
                <dd class="col-sm-9">{{$book->bok_published_date}}</dd>
            </dl> 
        </div>
        <!-- /.content -->
    </div>
</div>
<!-- /.Main Section -->
@endsection