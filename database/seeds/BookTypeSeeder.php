<?php

use Illuminate\Database\Seeder;

class BookTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('book_types')->insert([
            'bot_name' => 'Novel',
        ]);

        DB::table('book_types')->insert([
            'bot_name' => 'Comic',
        ]);

        DB::table('book_types')->insert([
            'bot_name' => 'Motivation',
        ]);
    }
}
