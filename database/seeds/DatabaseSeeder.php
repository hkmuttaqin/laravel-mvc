<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PublisherSeeder::class);
        $this->call(WriterSeeder::class);
        $this->call(BookTypeSeeder::class);
        $this->call(BookSeeder::class);
    }
}
