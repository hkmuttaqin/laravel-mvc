<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            'bok_book_type_id' => 1,
            'bok_writer_id' => 1,
            'bok_publisher_id' => 1,
            'bok_name' => 'Laskar Pelangi',
            'bok_stock' => 5,
            'bok_published_date'  => '2020-01-01',
        ]);

        DB::table('books')->insert([
            'bok_book_type_id' => 1,
            'bok_writer_id' => 1,
            'bok_publisher_id' => 1,
            'bok_name' => 'Sang Pemimpi',
            'bok_stock' => 4,
            'bok_published_date'  => '2020-02-02',
        ]);

        DB::table('books')->insert([
            'bok_book_type_id' => 1,
            'bok_writer_id' => 2,
            'bok_publisher_id' => 1,
            'bok_name' => 'Negeri 5 Menara',
            'bok_stock' => 7,
            'bok_published_date'  => '2020-03-03',
        ]);

        DB::table('books')->insert([
            'bok_book_type_id' => 1,
            'bok_writer_id' => 3,
            'bok_publisher_id' => 2,
            'bok_name' => '99 Cahaya Langit Eropa',
            'bok_stock' => 9,
            'bok_published_date'  => '2020-04-04',
        ]);

        DB::table('books')->insert([
            'bok_book_type_id' => 2,
            'bok_writer_id' => 4,
            'bok_publisher_id' => 3,
            'bok_name' => 'Si Juki',
            'bok_stock' => 10,
            'bok_published_date'  => '2020-05-05',
        ]);

        DB::table('books')->insert([
            'bok_book_type_id' => 3,
            'bok_writer_id' => 4,
            'bok_publisher_id' => 4,
            'bok_name' => 'The Magic of Thinking Big',
            'bok_stock' => 11,
            'bok_published_date'  => '2020-06-06',
        ]);
    }
}
