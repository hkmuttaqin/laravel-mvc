<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PublisherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 5 ; $i++) { 
            DB::table('publishers')->insert([
                'pub_name' => 'Penerbit '.$i,
            ]);
        }
    }
}
