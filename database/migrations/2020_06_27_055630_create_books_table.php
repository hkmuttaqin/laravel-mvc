<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('bok_id');
            $table->foreignId('bok_book_type_id');
            $table->foreignId('bok_writer_id');
            $table->foreignId('bok_publisher_id');
            $table->string('bok_name');
            $table->integer('bok_stock');
            $table->date('bok_published_date');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('bok_book_type_id')->references('bot_id')->on('book_types');
            $table->foreign('bok_writer_id')->references('wrt_id')->on('writers');
            $table->foreign('bok_publisher_id')->references('pub_id')->on('publishers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
