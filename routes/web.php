<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/about', function () {
    return view('about');
});

Route::group(['prefix' => 'books'], function () {
    Route::get('/', 'BookController@index');
    Route::post('/', 'BookController@store');
    Route::put('/', 'BookController@update');
    Route::get('/create', 'BookController@create');
    Route::get('/{book}', 'BookController@show');
    Route::get('/{book}/edit', 'BookController@edit');
    Route::get('/{book}/destroy', 'BookController@destroy');
});
